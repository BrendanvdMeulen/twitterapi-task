<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Input;

class IndexController extends BaseController
{
    public function getTimeLine() {
        $tweets = [];
        $errorMessage = null;

        // Retrieve tweets
        try {
            $tweets = \Twitter::getHomeTimeline(['count' => 20, 'format' => 'array']);
        } catch ( \Exception $e){
            $errorMessage = $e->getMessage();
        }

//        echo '<pre>';
//        var_dump($tweets);
//        echo '</pre>';
//        die();

        $data = [
            'tweets'=> $tweets,
            'errorMessage'=> $errorMessage,
        ];

        return view('timeline')->with($data);
    }
}