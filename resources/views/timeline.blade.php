<!DOCTYPE html>
<html>
    <head>
        <title>Twitter API</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    </head>

    <body>
        <div class="container">
            <div class="content">
                <?php
                    if (!is_null($errorMessage)):
                ?>
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            <?= $errorMessage ?>
                        </div>
                    <?php
                        endif;
                    ?>
                <?php
                    foreach ($tweets as $tweet):
                        $text = $tweet['text'];
                        $name = $tweet['user']['name'];
                        $userImage = $tweet['user']['profile_image_url'];
                ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src='<?= $userImage ?>' />

                        <p>
                            <?= $name ?>
                        </p>

                        <p>
                            <?= $text ?>
                        </p>
                    </div>
                </div>
                <?php
                    endforeach;
                ?>
            </div>
        </div>
    </body>
</html>
